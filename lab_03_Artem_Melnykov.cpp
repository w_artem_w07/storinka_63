﻿#include <iostream>
#include <math.h>

#define PUT_DELIM(x, h, b) (x)+(h) >= (b) ? '\n' : ' '

#define COND1(x) x < 0
#define FUNC1(x) -pow(x,2)+1/x

#define COND2(x) 0 < x && x < 3
#define FUNC2(x) sqrt(pow(x,2)+1)

#define COND3(x) x=>3
#define FUNC3(x) x-3

#define FUNC(x) COND1(x) ? FUNC1(x): (COND2(x) ? FUNC2(x) :\FUNC3(x))

using namespace std;

int main()
{
	double a, b, x, h;
	cout << "enter your a,b,h values (0<=x<=pi)" << endl;
	printf("x:\t");
	for (x = a; x <= b; x += h)

	{
		printf("%6.2f%c", x, PUT_DELIM(x, h, b));
	}
	printf("y:\t");
	for (x = a; x <= b; x += h)
	{
		printf("%6.3f%c", FUNC(x), PUT_DELIM(x, h, b));
	}
	return 0;
